package Components;

import static Components.ComponentType.componentType.BANNER;
import static Components.ComponentType.componentType.BANNERIMAGE;
import static Components.ComponentType.componentType.HEADER;
import static Components.ComponentType.componentType.LINK;
import static Components.ComponentType.componentType.LIST;
import static Components.ComponentType.componentType.PARAGRAPH;
import static Components.ComponentType.componentType.SLIDESHOW;
import static Components.ComponentType.componentType.VIDEO;
import java.util.HashMap;

/**
 *
 * @author JL
 */
public class ComponentType {
    
    public static HashMap componentTypeMap = new HashMap();
    public static enum componentType{
        
        PARAGRAPH, HEADER, LIST, VIDEO, IMAGE, SLIDESHOW, LINK, BANNER,
        BANNERIMAGE
    };
    
    ComponentType(){
    
        initComponentTypeMap();
    }
    
    public final void initComponentTypeMap(){
        componentTypeMap.put(HEADER       , "header");
        componentTypeMap.put(PARAGRAPH    , "paragraph");
        componentTypeMap.put(LIST         , "list");
        componentTypeMap.put(VIDEO        , "video");
        componentTypeMap.put(SLIDESHOW    , "slideshow");
        componentTypeMap.put(LINK         , "link");
        componentTypeMap.put(BANNER       , "banner");
        componentTypeMap.put(BANNERIMAGE  , "bannerimage");

    }
    
}
