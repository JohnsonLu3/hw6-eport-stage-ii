package View;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.ScrollPane;
import static javafx.scene.input.DataFormat.URL;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 *
 * @author JL
 */
public class SiteView {

    StackPane webPane = new StackPane();
    ScrollPane webScroll = new ScrollPane();
    WebView browser = new WebView();
    WebEngine webEngine = browser.getEngine();

    public SiteView() {
        
        String webPath = "./Sites/Demo/index.html";
        URL load = null;
        try {
            load = new File(webPath).toURI().toURL();
        } catch (MalformedURLException ex) {
            Logger.getLogger(SiteView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        webEngine.load(load.toExternalForm());
        webScroll.setFitToHeight(true);
        webScroll.setFitToWidth(true);

        webScroll.setContent(browser);


        webPane.getChildren().add(webScroll);
        webPane.setPrefHeight(1000);
    }

    public Pane getWebPane() {

        return webPane;
    }

}
