package View.ToolPrompts;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.ChoiceDialog;

/**
 *
 * @author JL
 */
public class ColorPrompt {

    private String color = "Albany";

    /**
     * colorPicker A prompt that pops up when the users presses the color button
     * It will give the user a drop down that contains 5 different color schemes
     * from which the user can pick from. Hitting okay will return the picked
     * color and the cancel button will ignore any actions.
     *
     * @return String Color Scheme
     */
    public String colorPicker(String CS) {
        
        color = CS;
        
        List<String> choices = new ArrayList<>();
        choices.add("Albany");
        choices.add("Stony Brook");
        choices.add("Orange");
        choices.add("Pastel");
        choices.add("Green");

        ChoiceDialog<String> dialog = new ChoiceDialog<>(color, choices);
        dialog.setTitle("Choose Color");
        dialog.setHeaderText("Pick a Color");
        dialog.setContentText("Choose a Color Scheme for this page:");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            System.out.println("Your choice: " + result.get());
            color = result.get();
        } else {
            System.out.println("The user hit cancel");
        }

        return color;
    }

}
