package View.ToolPrompts;

import View.DialogTemplate;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMFRIMBUTTONS;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

/**
 *
 * @author JL
 */
public class ImagePrompt {

    public DialogTemplate imageDialog = new DialogTemplate();

    String imageName = "";
    int iamgeHeight = 600;
    int imageWidth = 800;
    TextField height = new TextField();
    TextField width = new TextField();
    public Button addImage = new Button("Choose Image...");

    public HBox buttons = new HBox();
    public Button okay = new Button("Okay");
    public Button cancel = new Button("Cancel");

    HBox deminsionsBox = new HBox();

    public String imageEditor() {

        height.setPromptText("Height");
        width.setPromptText("Width");

        imageDialog.setStageTitle("Add Image Component");
        imageDialog.setHeader("Choose a Image to add");
        deminsionsBox.getChildren().add(addImage);
        deminsionsBox.getChildren().add(height);
        //deminsionsBox.getChildren().add(new Label("X"));
        deminsionsBox.getChildren().add(width);
        deminsionsBox.setAlignment(Pos.CENTER);
        deminsionsBox.setSpacing(5);
        deminsionsBox.setPadding(new Insets(20, 0, 0, 0));

        //RADIO BUTTONS
        HBox radioButtons = new HBox();
        ToggleGroup group = new ToggleGroup();
        RadioButton Left = new RadioButton();
        RadioButton Center = new RadioButton();
        RadioButton Right = new RadioButton();

        Label leftLabel = new Label("Left");
        Label centerLabel = new Label("Center");
        Label rightLabel = new Label("Right");

        leftLabel.setPadding(new Insets(0, 15, 0, 0));
        centerLabel.setPadding(new Insets(0, 15, 0, 0));
        rightLabel.setPadding(new Insets(0, 15, 0, 0));

        //Added the radio buttons to a togglable group
        Left.setToggleGroup(group);
        Center.setToggleGroup(group);
        Right.setToggleGroup(group);

        radioButtons.getChildren().add(Left);
        radioButtons.getChildren().add(leftLabel);
        radioButtons.getChildren().add(Center);
        radioButtons.getChildren().add(centerLabel);
        radioButtons.getChildren().add(Right);
        radioButtons.getChildren().add(rightLabel);
        radioButtons.setAlignment(Pos.CENTER);
        radioButtons.setPadding(new Insets(15, 0, 15, 0));
        imageDialog.popUpContainer.getChildren().add(radioButtons);

        imageDialog.popUpContainer.getChildren().add(deminsionsBox);

        buttons.getChildren().add(okay);
        buttons.getChildren().add(cancel);
        buttons.getStyleClass().add(CSS_COMFRIMBUTTONS);
        buttons.setSpacing(20);
        buttons.setAlignment(Pos.CENTER);

        imageDialog.popUpContainer.getChildren().add(buttons);

        imageDialog.showPopUp();

        //ON CLOSE handler
        imageDialog.popUpStage.setOnCloseRequest(e -> {

        });

        return imageName;
    }

    public String getImageName() {
        return imageName;
    }

    public int getImageHeight() {
        return iamgeHeight;
    }

    public int getImageWidth() {
        return imageWidth;
    }
}
