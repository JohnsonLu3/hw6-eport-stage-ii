package View.ToolPrompts;

import View.DialogTemplate;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMFRIMBUTTONS;
import static eportfoliogenerator.StartupConstantsEport.CSS_TEXTFIELD;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author JL
 */
public class HyperLinkPrompt {

    public DialogTemplate linkEditor = new DialogTemplate();

    public HBox buttons = new HBox();
    public String linkAddress;
    public String hyperText;
    
    private TextArea paragraph = new TextArea();
    private HBox linkInput = new HBox();
    private Label linkLabel = new Label("HyperLink Address: ");
    private TextField linkField = new TextField();

    public Button add = new Button("add");
    public Button cancel = new Button("Cancel");
    
    public void linkEditor(){
    
        linkEditor.setStageTitle("Add Hyper Link");
        linkEditor.setHeader("Select Text to be linked and enter address below");
        
        linkInput.getChildren().add(linkLabel);
        linkInput.getChildren().add(linkField);
        linkInput.setAlignment(Pos.CENTER);
        linkInput.setSpacing(10);
        linkInput.setPadding(new Insets(25, 0, 0, 0));
        
        buttons.getChildren().add(add);
        buttons.getChildren().add(cancel);
        buttons.getStyleClass().add(CSS_COMFRIMBUTTONS);
        buttons.setSpacing(20);
        buttons.setAlignment(Pos.CENTER);        
        
                

        paragraph.getStyleClass().add(CSS_TEXTFIELD);
        VBox text = new VBox();
        text.setPadding(new Insets(10, 40, 0, 40));
        text.getChildren().add(paragraph);
        
        linkEditor.popUpContainer.getChildren().add(text);
        linkEditor.popUpContainer.getChildren().add(linkInput);
        linkEditor.popUpContainer.getChildren().add(buttons);
        
        
        linkEditor.showPopUp();
    }
}
