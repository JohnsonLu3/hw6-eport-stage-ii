package View.ToolPrompts;

import View.DialogTemplate;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMFRIMBUTTONS;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 *
 * @author JL
 */
public class VideoPrompt {

    public DialogTemplate videoDialog = new DialogTemplate();

    String videoName = "";
    int videoHeight = 600;
    int videoWidth = 800;
    TextField height = new TextField();
    TextField width = new TextField();
    public Button addVideo = new Button("Choose Video...");

    public HBox buttons = new HBox();
    public Button okay = new Button("Okay");
    public Button cancel = new Button("Cancel");

    HBox deminsionsBox = new HBox();

    public String videoEditor() {

        height.setPromptText("Height");
        width.setPromptText("Width");

        videoDialog.setStageTitle("Add Video Component");
        videoDialog.setHeader("Choose a video to add");
        deminsionsBox.getChildren().add(addVideo);
        deminsionsBox.getChildren().add(height);
        //deminsionsBox.getChildren().add(new Label("X"));
        deminsionsBox.getChildren().add(width);
        deminsionsBox.setAlignment(Pos.CENTER);
        deminsionsBox.setSpacing(5);
        deminsionsBox.setPadding(new Insets(100, 0,0,0));


        videoDialog.popUpContainer.getChildren().add(deminsionsBox);

        buttons.getChildren().add(okay);
        buttons.getChildren().add(cancel);
        buttons.getStyleClass().add(CSS_COMFRIMBUTTONS);
        buttons.setSpacing(20);
        buttons.setAlignment(Pos.CENTER);

        videoDialog.popUpContainer.getChildren().add(buttons);

        videoDialog.showPopUp();

        //ON CLOSE handler
        videoDialog.popUpStage.setOnCloseRequest(e -> {

        });

        return videoName;
    }

    public String getVideoName() {
        return videoName;
    }

    public int getVideoHeight() {
        return videoHeight;
    }

    public int getVideoWidth() {
        return videoWidth;
    }

}
