package View;

import static eportfoliogenerator.StartupConstantsEport.CSS_FILETOOLBAR;
import static eportfoliogenerator.StartupConstantsEport.EXITICONPATH;
import static eportfoliogenerator.StartupConstantsEport.EXPORTICONPATH;
import static eportfoliogenerator.StartupConstantsEport.NEWFILEICONPATH;
import static eportfoliogenerator.StartupConstantsEport.OPENICONPATH;
import static eportfoliogenerator.StartupConstantsEport.SAVEASICONPATH;
import static eportfoliogenerator.StartupConstantsEport.SAVEICONPATH;
import javafx.scene.image.Image;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;

/**
 *
 * @author JL
 */
public class FileToolbar_view {
    
    public Button newButton = new Button();
    public Button saveButton = new Button();
    public Button saveAsButton = new Button();
    public Button loadButton = new Button();
    public Button exportButton = new Button();
    public Button exitButton = new Button();
    
    
    FlowPane fileFlowPane = new FlowPane();
    HBox fileToolbar = new HBox();
    ToolBar toolbar = new ToolBar();
    
    
    public FileToolbar_view(){
        System.out.println(NEWFILEICONPATH);
        
        initButton(newButton,NEWFILEICONPATH);
        initButton(saveButton,SAVEICONPATH);
        initButton(saveAsButton,SAVEASICONPATH);
        initButton(loadButton, OPENICONPATH);
        initButton(exportButton, EXPORTICONPATH);
        initButton(exitButton, EXITICONPATH);
        
        newButton.setTooltip( new Tooltip("New ePortfolio"));
        saveButton.setTooltip( new Tooltip("Save ePortfolio"));
        saveAsButton.setTooltip( new Tooltip("Save As ePortfolio"));
        loadButton.setTooltip( new Tooltip("Load ePortfolio"));
        exportButton.setTooltip( new Tooltip("Eport ePortfolio"));
        exitButton.setTooltip( new Tooltip("Exit ePortfolio"));
        
        initFileToolbar();
        initFileFlowPane();
    }
    
    public void initButton( Button butt, String iconPath){
        
        Image buttIcon = new Image("file:" + iconPath);
        butt.setGraphic(new ImageView(buttIcon));
        butt.setVisible(true);
        
        
    }
    
    public void initFileToolbar(){
        fileToolbar.getChildren().add(newButton);
        fileToolbar.getChildren().add(saveButton);
        fileToolbar.getChildren().add(saveAsButton);
        fileToolbar.getChildren().add(loadButton);
        fileToolbar.getChildren().add(exportButton);
        fileToolbar.getChildren().add(exitButton);
        
        toolbar.getItems().add(fileToolbar);
    
    }
    
    public void initFileFlowPane(){
    
        fileFlowPane.getChildren().add(fileToolbar);
        fileFlowPane.getStyleClass().add(CSS_FILETOOLBAR);
    }
    
    
    public ToolBar getToolBar(){
        return toolbar;
    }
}
