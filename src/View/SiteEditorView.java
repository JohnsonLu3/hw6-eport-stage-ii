package View;


import static eportfoliogenerator.StartupConstantsEport.CSS_COMPONENT;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMPONENTCONT;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMPONENTTYPE;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author JL
 */
public class SiteEditorView {
    
    Pane siteEditor = new Pane();
    ScrollPane siteScroll = new ScrollPane();
    VBox components = new VBox();
    VBox workSpacePane = new VBox();
    
    TabPane pageTabs = new TabPane();
    
    public SiteEditorView(){
        
        
        //TEST//
        //
           components.getChildren().add(testComponents("Header"));
           components.getChildren().add(testComponents("Paragraph"));
           components.getChildren().add(testComponents("List"));
           components.setSpacing(15);
        //
        //END OF TEST
        siteScroll.setFitToHeight(true);
        siteScroll.setFitToWidth(true);
        siteScroll.setContent(components);
        siteScroll.setPadding(new Insets(15,0 ,0, 0));
        
        VBox.setVgrow(pageTabs, Priority.ALWAYS);
        pageTabs.setPrefWidth(3000);
        initPageTabs();
        
        workSpacePane.getChildren().add(pageTabs);
        workSpacePane.getChildren().add(siteScroll);
        siteEditor.getChildren().add(workSpacePane);
        
    }
    
    public void addComponent(){
        
    }
    
    public void removeComponent(){
    
    }
    
    public Pane getSiteEditor(){
        return siteEditor;
    }
    
    
    //TEST//
    //
    public VBox testComponents(String type){
        VBox testComp = new VBox();
        
        testComp.setPrefWidth(2000);
        Label componentType = new Label(type);
        componentType.getStyleClass().add(CSS_COMPONENTTYPE);
        
        testComp.getStyleClass().add(CSS_COMPONENT);
        testComp.getChildren().add(componentType);
        
        
        
        testComp.setOnMouseClicked(e->{
            testComp.setStyle("-fx-background-color: #AAB9C9");
        });
        
        
        
        
        return testComp;
        
    }
    ////END OF TEST//
    
    public void initPageTabs(Tab page){
    
        pageTabs.getTabs().add(page);
        page.setContent(siteScroll);
    }
    
    //TEST//
    public void initPageTabs(){
    
        Tab page1 = new Tab();
        Tab page2 = new Tab();
       
        
        page1.setText("Page 1");
        page2.setText("Page 2");
    
        
        page1.setClosable(false);
        page2.setClosable(false);
      
        
        page1.setContent(siteScroll);
        page2.setContent(test());
        //page3.setContent(siteScroll);
        
        pageTabs.getTabs().add(page1);
        pageTabs.getTabs().add(page2);
      
        
    }
    
    public ScrollPane test(){
    
        ScrollPane SP = new ScrollPane();
        VBox testStuff = new VBox();
        
        testStuff.getChildren().add(testComponents("Header"));
        testStuff.getChildren().add(testComponents("Paragraph"));
        testStuff.getChildren().add(testComponents("Video"));
        testStuff.getChildren().add(testComponents("Image"));
        testStuff.getChildren().add(testComponents("Header"));
        testStuff.getChildren().add(testComponents("List"));
        
        testStuff.setSpacing(15);
        SP.setContent(testStuff);
        SP.setPadding(new Insets(15,0 ,0, 0));
        
    
        
        
        return SP;
    }
    
    
    
}
