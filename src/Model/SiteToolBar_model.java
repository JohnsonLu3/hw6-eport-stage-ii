package Model;

import Controller.SiteTbController;
import View.SiteToolbar_view;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author JL
 */
public class SiteToolBar_model {
    
    private SiteToolbar_view siteTB = new SiteToolbar_view();
    private SiteTbController siteController = new SiteTbController(siteTB);
    
    public SiteToolBar_model(){
        
    }
    
    public VBox getSiteTB(){
        
        return siteTB.getSiteBar();
    }
}
