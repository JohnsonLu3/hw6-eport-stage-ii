package Model;

import View.SiteEditorView;
import View.SiteView;
import View.WorkSpaceMode_view;
import javafx.scene.control.TabPane;

/**
 *
 * @author JL
 */
public class WorkSpaceMode_model {

    WorkSpaceMode_view WSMTB = new WorkSpaceMode_view();


    public WorkSpaceMode_model() {


        SiteView SVP = new SiteView();
        SiteEditorView SEV = new SiteEditorView();
        
        WSMTB.editortab.setContent(SEV.getSiteEditor());
        WSMTB.sitetab.setContent(SVP.getWebPane());
    }

    public TabPane getTabPane() {
        return WSMTB.getTabPane();
    }
}
