package Controller;

import View.FileToolbar_view;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author JL
 */
public class FileTbController {

    FileToolbar_view FileTB;

    public FileTbController(FileToolbar_view FTBV) {
        FileTB = FTBV;
        initFileHandlers();
    }

    private void initFileHandlers() {

        FileTB.newButton.setOnAction(e -> {
            Alert comfirmNew = new Alert(AlertType.INFORMATION);
            comfirmNew.setTitle("New ePortFolio");
            comfirmNew.setHeaderText(null);
            comfirmNew.setContentText("A new ePortFolio has been created");

            comfirmNew.showAndWait();
        });

        FileTB.saveButton.setOnAction(e -> {
            TextInputDialog comfirmSave = new TextInputDialog();
            comfirmSave.setTitle("Save");
            comfirmSave.setHeaderText("Please Enter a Name to Save Your File Under");
            comfirmSave.setContentText("Save:");

            Optional<String> result = comfirmSave.showAndWait();
            if (result.isPresent()) {
                ////TODO
            }

        });

        FileTB.saveAsButton.setOnAction(e -> {
            TextInputDialog comfirmSaveAs = new TextInputDialog();
            comfirmSaveAs.setTitle("Save As");
            comfirmSaveAs.setHeaderText("Please Enter a Name to Save As");
            comfirmSaveAs.setContentText("Save As:");

            Optional<String> result = comfirmSaveAs.showAndWait();
            if (result.isPresent()) {
                ////TODO
            }

        });

        FileTB.loadButton.setOnAction(e -> {
            Alert comfirmLoad = new Alert(AlertType.INFORMATION);
            Stage fileStage = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            fileChooser.showOpenDialog(fileStage);
            comfirmLoad.setTitle("Load");
            comfirmLoad.setHeaderText(null);
            comfirmLoad.setContentText("EportFolio " + "EPORTFOLIO NAME HERE" + " has been loaded! DONT SHOW IF CANCELED OR INVILAD");

            comfirmLoad.showAndWait();
        });

        FileTB.exitButton.setOnAction(e -> {

            Alert comfirmExit = new Alert(AlertType.CONFIRMATION);
            comfirmExit.setTitle("Exit");
            comfirmExit.setHeaderText(null);
            comfirmExit.setContentText("Do you wish to exit?");

            Optional<ButtonType> result = comfirmExit.showAndWait();
            if (result.get() == ButtonType.OK) {
                 // ... user chose OK
            } else {
                // ... user chose CANCEL or closed the dialog
            }
        });

        FileTB.exportButton.setOnAction(e -> {

            Alert comfirmExport = new Alert(AlertType.INFORMATION);
            comfirmExport.setTitle("Exported");
            comfirmExport.setHeaderText(null);
            comfirmExport.setContentText("The ePortFolio has been Exported!");

            comfirmExport.showAndWait();
        });
    }
}
