package Controller;

import View.WorkspaceToolBar_view;
import View.ToolPrompts.ColorPrompt;
import View.ToolPrompts.FontPrompt;
import View.ToolPrompts.FooterPrompt;
import View.ToolPrompts.HyperLinkPrompt;
import View.ToolPrompts.ImagePrompt;
import View.ToolPrompts.LayoutPrompt;
import View.ToolPrompts.TextEditPrompt;
import View.ToolPrompts.VideoPrompt;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ssm.SlideShowMaker;

/**
 *
 * @author JL
 */
public class WorkspaceToolBar_controller {

    WorkspaceToolBar_view workspaceTB;
    private String layoutChoice = "Article";
    private String colorChoice = "Albany";
    private String fontChoice = "Economica";
    private String footerText = "";

    public WorkspaceToolBar_controller(WorkspaceToolBar_view WSTB) {

        workspaceTB = WSTB;
        initHandlers();
    }

    private void initHandlers() {

        workspaceTB.bannerImage.setOnAction(e -> {
            Stage fileStage = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Banner Image File");
            fileChooser.showOpenDialog(fileStage);
        });

        workspaceTB.layoutButt.setOnAction(e -> {

            LayoutPrompt LOP = new LayoutPrompt();
            layoutChoice = LOP.layoutPicker(layoutChoice);

            System.out.println("Current Layout : " + layoutChoice);

        });

        workspaceTB.colorButt.setOnAction(e -> {

            ColorPrompt CSP = new ColorPrompt();
            colorChoice = CSP.colorPicker(colorChoice);

            System.out.println("Current Color Scheme : " + colorChoice);

        });

        workspaceTB.fontButt.setOnAction(e -> {

            FontPrompt FCP = new FontPrompt();

            fontChoice = FCP.fontPicker(fontChoice);
            System.out.println("Current Font : " + fontChoice);

        });

        workspaceTB.textButt.setOnAction(e -> {
            textPrompt();

        });

        workspaceTB.videoButt.setOnAction(e -> {
            videoPrompt();

        });

        workspaceTB.imgButt.setOnAction(e -> {
            imgPrompt();
        });

        workspaceTB.slideButt.setOnAction(e -> {
            slideShowPrompt();
        });

        workspaceTB.linkButt.setOnAction(e -> {
            linkPrompt();
        });

        workspaceTB.editButt.setOnAction(e -> {
            //if selected component type is equal to stuff then launch corresponing prompted with info filled in
            textPrompt();
        });

        workspaceTB.removeButt.setOnAction(e -> {
            Alert comfirmRemove = new Alert(AlertType.CONFIRMATION);
            comfirmRemove.setTitle("Confirm Remove");
            comfirmRemove.setHeaderText("Remove Selected Component(s)?");
            comfirmRemove.setContentText("Do you wish to remove the following selected component(s)?");

            Optional<ButtonType> result = comfirmRemove.showAndWait();
            if (result.get() == ButtonType.OK) {
                // ... user chose OK
            } else {
                // ... user chose CANCEL or closed the dialog
            }
        });

        workspaceTB.footer.setOnAction(e -> {
            footerPrompt();
        });

    }

    //Prompt Init
    public void textPrompt() {
        TextEditPrompt TEP = new TextEditPrompt();

        TEP.textEditor();

        TEP.okay.setOnAction(ee -> {
            TEP.textEditorDialog.closePopUp();
        });

        TEP.cancel.setOnAction(eee -> {
            TEP.textEditorDialog.closePopUp();
        });
    }

    public void imgPrompt() {

        ImagePrompt IEP = new ImagePrompt();

        IEP.imageEditor();

        IEP.okay.setOnAction(ee -> {
            IEP.imageDialog.closePopUp();
        });

        IEP.cancel.setOnAction(eee -> {
            IEP.imageDialog.closePopUp();
        });

        IEP.addImage.setOnAction(i -> {
            Stage fileStage = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Image File");
            fileChooser.showOpenDialog(fileStage);
        });
    }

    public void videoPrompt() {

        VideoPrompt VEP = new VideoPrompt();

        VEP.videoEditor();

        VEP.okay.setOnAction(ee -> {
            VEP.videoDialog.closePopUp();
        });

        VEP.cancel.setOnAction(eee -> {
            VEP.videoDialog.closePopUp();
        });

        VEP.addVideo.setOnAction(i -> {
            Stage fileStage = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Video File");
            fileChooser.showOpenDialog(fileStage);
        });
    }

    public void footerPrompt() {

        FooterPrompt FEP = new FooterPrompt();

        FEP.footerEditor(footerText);

        FEP.okay.setOnAction(e -> {
            FEP.getFooterText();
            footerText = FEP.getFooterText();
            FEP.footerEditorDialog.closePopUp();
        });

        FEP.cancel.setOnAction(e -> {
            FEP.comfrimExit();
        });

        System.out.println(FEP.getFooterText());
    }

    public void linkPrompt() {
        HyperLinkPrompt HLEP = new HyperLinkPrompt();
        System.out.print("Jello Link");
        HLEP.linkEditor();

        HLEP.add.setOnAction(e -> {
            HLEP.linkEditor.closePopUp();
        });

        HLEP.cancel.setOnAction(e -> {
            HLEP.linkEditor.closePopUp();
        });
    }

    public void slideShowPrompt() {
        //slideShow_model SSM = new slideShow_model();
        SlideShowMaker SSM = new SlideShowMaker();
        Stage slideShowEditor = new Stage();
        try {
            SSM.start(slideShowEditor);
        } catch (Exception ex) {
            Logger.getLogger(WorkspaceToolBar_controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
