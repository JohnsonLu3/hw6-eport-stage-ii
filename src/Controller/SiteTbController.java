package Controller;

import View.SiteToolbar_view;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 *
 * @author JL
 */
public class SiteTbController {

    SiteToolbar_view siteTB;

    public SiteTbController(SiteToolbar_view STBV) {

        siteTB = STBV;
        initSiteHandlers();
    }

    private void initSiteHandlers() {

        siteTB.addPageButt.setOnAction(e -> {
            Alert comfirmSelect = new Alert(Alert.AlertType.INFORMATION);
            comfirmSelect.setTitle("New Page");
            comfirmSelect.setHeaderText(null);
            comfirmSelect.setContentText("New Page has been added.");

            comfirmSelect.showAndWait();

        });

        siteTB.removePageButt.setOnAction(e -> {
            Alert comfirmRemove = new Alert(AlertType.CONFIRMATION);
            comfirmRemove.setTitle("Remove Page");
            comfirmRemove.setHeaderText(null);
            comfirmRemove.setContentText("Do you wish to remove this page?");

            comfirmRemove.showAndWait();
        });

        siteTB.selectPageButt.setOnAction(e -> {
            Alert comfirmSelect = new Alert(Alert.AlertType.INFORMATION);
            comfirmSelect.setTitle("Multi Select");
            comfirmSelect.setHeaderText(null);
            comfirmSelect.setContentText("You Can Now Select multiple pages.");

            comfirmSelect.showAndWait();
        });

    }

}
