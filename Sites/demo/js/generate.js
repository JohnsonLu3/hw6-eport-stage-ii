/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var keyQueue = [];
var componentQueue = [];
var page;
var pageName;
var sectionCount = 0;

var head = document.getElementsByTagName('head')[0];
var linkLO = document.createElement('link');
var linkColor = document.createElement('link');
var linkFont = document.createElement('link');


$(function () {

    pageName = location.pathname.substring(location.pathname.lastIndexOf("/") + 1);
    
    //set up css links
    setUpLinks();

    page = document.getElementById("content");
    page.innerHTML = "";

    $.getJSON('js/ePortTest.json', function (data) {
        $.each(data, function (pageObj, htmlName) {

            if (pageObj === pageName) {
                var pageComponents = data;
                
                $.each(pageComponents[pageName], function (type, content) {
                    lookUpComponent(type, content);
                });
                
            }


        });


    });
    page.innerHTML += "</section>";
    page.innerHTML += "</div>";

});

/**
 * Sets up CSS file links
 * @returns {undefined}
 */
function setUpLinks() {

    linkLO.rel = 'stylesheet';
    linkLO.type = 'text/css';
    linkLO.href = 'css/layout/';
    linkLO.media = 'all';

    linkColor.rel = 'stylesheet';
    linkColor.type = 'text/css';
    linkColor.href = 'css/colors/';
    linkColor.media = 'all';

    linkFont.rel = 'stylesheet';
    linkFont.type = 'text/css';
    linkFont.href = 'css/fonts/';
    linkFont.media = 'all';
}

/**
 * Sets up Compontents using Json values from the key "Components"
 * @returns {undefined}
 */
function setUpComponents() {
    console.log("KeyQueue Length :  " + keyQueue.length);
    queueLength = keyQueue.length;
    for (var i = 0; i < queueLength; i++) {
        
        var componentNum = "";
        var componentInfo = "";
        
        componentNum = keyQueue.shift();
        componentInfo = componentQueue.shift();
        
        page.innerHTML += "<div id=\"section"+ componentNum +"\" class=\"section\">" + componentInfo +"</div>";
        

    }
}

/**
 * This function uses a switch statement and key value pairs
 * to find if the value is a layout, color, font, Nav, or ECT.
 * if the value is a Component then the values of the Component
 * object is then pushed onto a queue for setUpComponents()
 * @param {type} cType
 * @param {type} cContent
 * @returns {undefined}
 */
function lookUpComponent( cType , cContent){
    
    type = cType;
    content = cContent;
    
    switch (type) {
                        case 'layout':
                            linkLO.href += content;
                            head.appendChild(linkLO);
                            break;
                        case 'color':
                            linkColor.href += content;
                            head.appendChild(linkColor);
                            break;
                        case 'font' :
                            linkFont.href += content;
                            head.appendChild(linkFont);
                            break;
                        case 'Nav' :
                            page.innerHTML += "<div id=\"content\">";
                            page.innerHTML += content;
                            break;
                        case 'banner' :
                            page.innerHTML += "<div id=\"banner\"><h1>" + content + "</h1></div>";
                            break;
                        case 'bannerImage' :
                            page.innerHTML += "<img src=img/" + content + "  alt=\"BannerImage\" id=\"bannerImage\" />";
                            break;
                        case 'Components' :
                            
                            page.innerHTML += "<section>";

                            $.each(content, function (componentType, componentContent) {
                                
                                keyQueue.push(componentType);
                                componentQueue.push(componentContent);
                                console.log(componentType  + "   DDDD");
                            });

                            setUpComponents();

                            break;
                        case 'footer' :
                            page.innerHTML += "<footer>" + content + "</footer>";
                            break;
                        default:
                            break;
                    }
}